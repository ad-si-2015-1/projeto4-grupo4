
package webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InicializaPalavrasResponse_QNAME = new QName("http://cliente/", "inicializaPalavrasResponse");
    private final static QName _IsJogoIniciou_QNAME = new QName("http://cliente/", "isJogoIniciou");
    private final static QName _GetTelaResponse_QNAME = new QName("http://cliente/", "getTelaResponse");
    private final static QName _SetJogoIniciouResponse_QNAME = new QName("http://cliente/", "setJogoIniciouResponse");
    private final static QName _RegistrarJogador_QNAME = new QName("http://cliente/", "registrarJogador");
    private final static QName _SetTelaResponse_QNAME = new QName("http://cliente/", "setTelaResponse");
    private final static QName _SetJogoIniciou_QNAME = new QName("http://cliente/", "setJogoIniciou");
    private final static QName _GetTela_QNAME = new QName("http://cliente/", "getTela");
    private final static QName _TrocarVez_QNAME = new QName("http://cliente/", "trocarVez");
    private final static QName _InicializaPalavras_QNAME = new QName("http://cliente/", "inicializaPalavras");
    private final static QName _RegistrarJogadorResponse_QNAME = new QName("http://cliente/", "registrarJogadorResponse");
    private final static QName _TratarMensagem_QNAME = new QName("http://cliente/", "tratarMensagem");
    private final static QName _IniciarJogoResponse_QNAME = new QName("http://cliente/", "iniciarJogoResponse");
    private final static QName _IsJogoIniciouResponse_QNAME = new QName("http://cliente/", "isJogoIniciouResponse");
    private final static QName _SetTela_QNAME = new QName("http://cliente/", "setTela");
    private final static QName _MontarTelaResponse_QNAME = new QName("http://cliente/", "montarTelaResponse");
    private final static QName _TratarMensagemResponse_QNAME = new QName("http://cliente/", "tratarMensagemResponse");
    private final static QName _TrocarVezResponse_QNAME = new QName("http://cliente/", "trocarVezResponse");
    private final static QName _MontarTela_QNAME = new QName("http://cliente/", "montarTela");
    private final static QName _IniciarJogo_QNAME = new QName("http://cliente/", "iniciarJogo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SetJogoIniciou }
     * 
     */
    public SetJogoIniciou createSetJogoIniciou() {
        return new SetJogoIniciou();
    }

    /**
     * Create an instance of {@link SetTelaResponse }
     * 
     */
    public SetTelaResponse createSetTelaResponse() {
        return new SetTelaResponse();
    }

    /**
     * Create an instance of {@link SetJogoIniciouResponse }
     * 
     */
    public SetJogoIniciouResponse createSetJogoIniciouResponse() {
        return new SetJogoIniciouResponse();
    }

    /**
     * Create an instance of {@link RegistrarJogador }
     * 
     */
    public RegistrarJogador createRegistrarJogador() {
        return new RegistrarJogador();
    }

    /**
     * Create an instance of {@link GetTelaResponse }
     * 
     */
    public GetTelaResponse createGetTelaResponse() {
        return new GetTelaResponse();
    }

    /**
     * Create an instance of {@link InicializaPalavrasResponse }
     * 
     */
    public InicializaPalavrasResponse createInicializaPalavrasResponse() {
        return new InicializaPalavrasResponse();
    }

    /**
     * Create an instance of {@link IsJogoIniciou }
     * 
     */
    public IsJogoIniciou createIsJogoIniciou() {
        return new IsJogoIniciou();
    }

    /**
     * Create an instance of {@link TratarMensagem }
     * 
     */
    public TratarMensagem createTratarMensagem() {
        return new TratarMensagem();
    }

    /**
     * Create an instance of {@link RegistrarJogadorResponse }
     * 
     */
    public RegistrarJogadorResponse createRegistrarJogadorResponse() {
        return new RegistrarJogadorResponse();
    }

    /**
     * Create an instance of {@link TrocarVez }
     * 
     */
    public TrocarVez createTrocarVez() {
        return new TrocarVez();
    }

    /**
     * Create an instance of {@link InicializaPalavras }
     * 
     */
    public InicializaPalavras createInicializaPalavras() {
        return new InicializaPalavras();
    }

    /**
     * Create an instance of {@link GetTela }
     * 
     */
    public GetTela createGetTela() {
        return new GetTela();
    }

    /**
     * Create an instance of {@link IniciarJogo }
     * 
     */
    public IniciarJogo createIniciarJogo() {
        return new IniciarJogo();
    }

    /**
     * Create an instance of {@link MontarTela }
     * 
     */
    public MontarTela createMontarTela() {
        return new MontarTela();
    }

    /**
     * Create an instance of {@link TrocarVezResponse }
     * 
     */
    public TrocarVezResponse createTrocarVezResponse() {
        return new TrocarVezResponse();
    }

    /**
     * Create an instance of {@link TratarMensagemResponse }
     * 
     */
    public TratarMensagemResponse createTratarMensagemResponse() {
        return new TratarMensagemResponse();
    }

    /**
     * Create an instance of {@link MontarTelaResponse }
     * 
     */
    public MontarTelaResponse createMontarTelaResponse() {
        return new MontarTelaResponse();
    }

    /**
     * Create an instance of {@link SetTela }
     * 
     */
    public SetTela createSetTela() {
        return new SetTela();
    }

    /**
     * Create an instance of {@link IsJogoIniciouResponse }
     * 
     */
    public IsJogoIniciouResponse createIsJogoIniciouResponse() {
        return new IsJogoIniciouResponse();
    }

    /**
     * Create an instance of {@link IniciarJogoResponse }
     * 
     */
    public IniciarJogoResponse createIniciarJogoResponse() {
        return new IniciarJogoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InicializaPalavrasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "inicializaPalavrasResponse")
    public JAXBElement<InicializaPalavrasResponse> createInicializaPalavrasResponse(InicializaPalavrasResponse value) {
        return new JAXBElement<InicializaPalavrasResponse>(_InicializaPalavrasResponse_QNAME, InicializaPalavrasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsJogoIniciou }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "isJogoIniciou")
    public JAXBElement<IsJogoIniciou> createIsJogoIniciou(IsJogoIniciou value) {
        return new JAXBElement<IsJogoIniciou>(_IsJogoIniciou_QNAME, IsJogoIniciou.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTelaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "getTelaResponse")
    public JAXBElement<GetTelaResponse> createGetTelaResponse(GetTelaResponse value) {
        return new JAXBElement<GetTelaResponse>(_GetTelaResponse_QNAME, GetTelaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetJogoIniciouResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "setJogoIniciouResponse")
    public JAXBElement<SetJogoIniciouResponse> createSetJogoIniciouResponse(SetJogoIniciouResponse value) {
        return new JAXBElement<SetJogoIniciouResponse>(_SetJogoIniciouResponse_QNAME, SetJogoIniciouResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrarJogador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "registrarJogador")
    public JAXBElement<RegistrarJogador> createRegistrarJogador(RegistrarJogador value) {
        return new JAXBElement<RegistrarJogador>(_RegistrarJogador_QNAME, RegistrarJogador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetTelaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "setTelaResponse")
    public JAXBElement<SetTelaResponse> createSetTelaResponse(SetTelaResponse value) {
        return new JAXBElement<SetTelaResponse>(_SetTelaResponse_QNAME, SetTelaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetJogoIniciou }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "setJogoIniciou")
    public JAXBElement<SetJogoIniciou> createSetJogoIniciou(SetJogoIniciou value) {
        return new JAXBElement<SetJogoIniciou>(_SetJogoIniciou_QNAME, SetJogoIniciou.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTela }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "getTela")
    public JAXBElement<GetTela> createGetTela(GetTela value) {
        return new JAXBElement<GetTela>(_GetTela_QNAME, GetTela.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrocarVez }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "trocarVez")
    public JAXBElement<TrocarVez> createTrocarVez(TrocarVez value) {
        return new JAXBElement<TrocarVez>(_TrocarVez_QNAME, TrocarVez.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InicializaPalavras }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "inicializaPalavras")
    public JAXBElement<InicializaPalavras> createInicializaPalavras(InicializaPalavras value) {
        return new JAXBElement<InicializaPalavras>(_InicializaPalavras_QNAME, InicializaPalavras.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrarJogadorResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "registrarJogadorResponse")
    public JAXBElement<RegistrarJogadorResponse> createRegistrarJogadorResponse(RegistrarJogadorResponse value) {
        return new JAXBElement<RegistrarJogadorResponse>(_RegistrarJogadorResponse_QNAME, RegistrarJogadorResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TratarMensagem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "tratarMensagem")
    public JAXBElement<TratarMensagem> createTratarMensagem(TratarMensagem value) {
        return new JAXBElement<TratarMensagem>(_TratarMensagem_QNAME, TratarMensagem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IniciarJogoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "iniciarJogoResponse")
    public JAXBElement<IniciarJogoResponse> createIniciarJogoResponse(IniciarJogoResponse value) {
        return new JAXBElement<IniciarJogoResponse>(_IniciarJogoResponse_QNAME, IniciarJogoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsJogoIniciouResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "isJogoIniciouResponse")
    public JAXBElement<IsJogoIniciouResponse> createIsJogoIniciouResponse(IsJogoIniciouResponse value) {
        return new JAXBElement<IsJogoIniciouResponse>(_IsJogoIniciouResponse_QNAME, IsJogoIniciouResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetTela }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "setTela")
    public JAXBElement<SetTela> createSetTela(SetTela value) {
        return new JAXBElement<SetTela>(_SetTela_QNAME, SetTela.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MontarTelaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "montarTelaResponse")
    public JAXBElement<MontarTelaResponse> createMontarTelaResponse(MontarTelaResponse value) {
        return new JAXBElement<MontarTelaResponse>(_MontarTelaResponse_QNAME, MontarTelaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TratarMensagemResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "tratarMensagemResponse")
    public JAXBElement<TratarMensagemResponse> createTratarMensagemResponse(TratarMensagemResponse value) {
        return new JAXBElement<TratarMensagemResponse>(_TratarMensagemResponse_QNAME, TratarMensagemResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrocarVezResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "trocarVezResponse")
    public JAXBElement<TrocarVezResponse> createTrocarVezResponse(TrocarVezResponse value) {
        return new JAXBElement<TrocarVezResponse>(_TrocarVezResponse_QNAME, TrocarVezResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MontarTela }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "montarTela")
    public JAXBElement<MontarTela> createMontarTela(MontarTela value) {
        return new JAXBElement<MontarTela>(_MontarTela_QNAME, MontarTela.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IniciarJogo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cliente/", name = "iniciarJogo")
    public JAXBElement<IniciarJogo> createIniciarJogo(IniciarJogo value) {
        return new JAXBElement<IniciarJogo>(_IniciarJogo_QNAME, IniciarJogo.class, null, value);
    }

}
