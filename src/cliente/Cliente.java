package cliente;
import java.util.Scanner;

import webservice.Jogo;
import webservice.JogoService;

public class Cliente {

	public static void main(String[] args) {
		JogoService jogoService = new JogoService();
		Jogo jogo = jogoService.getPort(Jogo.class);
		int versaoCliente = 0;

		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite seu nome: ");
		String nome = scanner.nextLine();
		System.out.println(jogo.registrarJogador(nome));
		
		while (!jogo.isJogoIniciou()) {
			
		}
		while (true) {
			String telaCliente[] = jogo.getTela().split(";"); 
			int versaoServidor = Integer.parseInt(telaCliente[1]);
			if (versaoServidor > versaoCliente) {
				versaoCliente = versaoServidor;
            	System.out.println(telaCliente[2]); //imprime o que tem que mostrar na tela
            }
            if (telaCliente[0].equals(nome)) { //verifica se a vez do jogador em quest�o
            	System.out.println("Sua vez");
            	String input = scanner.nextLine();
            	System.out.println(jogo.tratarMensagem(input));
            }
            
            //d� um descanso para verificar se mudou algo
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}