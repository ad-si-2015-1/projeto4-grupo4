package cliente;

import javax.jws.WebService;

import java.util.Random;
import java.util.Vector;

@WebService
public class Jogo {
	
	String jogador1;
	String jogador2;
	String jogador3;
	int pontosJogador1 = 0;
	int pontosJogador2 = 0;
	int pontosJogador3 = 0;
	int rodada = 0;
	String vezJogador;
	int versao = 0;
	public Vector<Palavra> palavras;
	Vector<Character> letrasUtilizadas = new Vector<Character>();
	Vector<Character> letrasAcertadas = new Vector<Character>();
	Palavra palavra;
	String tela = "";
	boolean jogoIniciou = false;
	
	public boolean isJogoIniciou() {
		return jogoIniciou;
	}

	public void setJogoIniciou(boolean jogoIniciou) {
		this.jogoIniciou = jogoIniciou;
	}

	public String getTela() {
		return tela;
	}

	public void setTela(String tela) {
		this.tela = tela;
	}

	public Jogo() {
		inicializaPalavras();
	}
	
	public String registrarJogador(String nome) {
		if (jogador1 == null) {
			jogador1 = nome;
		} else if (jogador2 == null) {
			jogador2 = nome;
		} else if (jogador3 == null) {
			jogador3 = nome;
			iniciarJogo();
		} else {
			return "Servidor lotado";
		}
		return "Bem vindo jogador " + nome;
	}
	
	public void iniciarJogo() {
		Random rand = new Random();
		int numIndexPalavra = rand.nextInt((getArrayPalavras().size()));
		palavra = getPalavra(numIndexPalavra);
		vezJogador = jogador1; //pega o nome do primeiro jogador pra iniciar a partida
		versao++;
		tela = montarTela(vezJogador, versao, palavra.mascararPalavra(palavra.getPalavra(), letrasAcertadas, letrasUtilizadas));
		rodada++;
		setJogoIniciou(true);
		//System.out.println(palavra.getPalavra() + "\n" + palavra.getCategoria());
	}
	
	public void trocarVez() {
		if (vezJogador.equals(jogador1)) {
			vezJogador = jogador2;
		} else if (vezJogador.equals(jogador2)) {
			vezJogador = jogador3;
		} else if (vezJogador.equals(jogador3)) {
			vezJogador = jogador1;
		}
	}
	
	public void pontuar (String jogador) {
		if (jogador.equals(jogador1)) {
			pontosJogador1++;
		} else if (jogador.equals(jogador2)) {
			pontosJogador2++;
		} else if (jogador.equals(jogador3)) {
			pontosJogador3++;
		}
	}
	
	public String montarTela(String jogadorDaVez, int versaoServidor, String palavraMascarada) {
		//variavel tela � separada por ';'. Ela tem o nome do jogador, a versao do servidor e o que deve ser printado na tela
		return jogadorDaVez + ";" + versaoServidor + ";" + palavraMascarada;
	}
	
	public String tratarMensagem (String comando) {
		String resposta = "";
		boolean letraRepetida = false;
		if(comando.length()==1){
			//verifica se o caracter de entrada do jogador esta contido na palavra em jogo
			char letraJogador = comando.charAt(0); 
			for(int i=0; i< letrasUtilizadas.size(); i++){
				if(letraJogador==letrasUtilizadas.get(i)){
					resposta = "Essa letra ja foi utilizada. Tente Novamente\n\r" + palavra.mascararPalavra(palavra.getPalavra(), letrasAcertadas, letrasUtilizadas);
					letraRepetida=true;
				}
			}
			if (!letraRepetida) {
				letrasUtilizadas.add(letraJogador);
				if(palavra.getPalavra().contains(Character.toString(letraJogador))){
					letrasAcertadas.add(letraJogador);
					resposta = "Voce acertou, jogue de novo por favor";
					versao++;
					tela = montarTela(vezJogador, versao, "\r\n------------------------------------------------------\r\nO jogador " + vezJogador + " acertou a letra '" + comando + "'.\n" + palavra.mascararPalavra(palavra.getPalavra(), letrasAcertadas,letrasUtilizadas));
				} else {
					resposta = "Voc� errou e perdeu a vez!";
					String jogadorErrou = vezJogador;
					trocarVez();
					versao++;
					tela = montarTela(vezJogador, versao, "\n------------------------------------------------------\r\nO jogador " + jogadorErrou + " errou a letra e perdeu a vez.\n" + palavra.mascararPalavra(palavra.getPalavra(), letrasAcertadas,letrasUtilizadas));
				}
			}
			letraRepetida=false;
		} else {
			
			//Verifica se a entrada do jogador � igual a palavra em jogo
			if(comando.equals(palavra.getPalavra())){
				resposta = "Voc� acertou a palavra! Parab�ns!";
				if (rodada < 6) {
					pontuar(vezJogador);
					String palavraAcertada = palavra.getPalavra();
					Random rand = new Random();
					int numIndexPalavra = rand.nextInt((getArrayPalavras().size()));
					palavra = getPalavra(numIndexPalavra);
					letrasUtilizadas.clear(); //limpa o vector para a nova palavra
					letrasAcertadas.clear(); //limpa o vector para a nova palavra
					versao++;
					tela = montarTela(vezJogador, versao, "\n------------------------------------------------------\r\nO vencedor foi o jogador " + vezJogador + "! PARABENS!!!\n"
							+ "A palavra era: "+ palavraAcertada +"\r\n"
							+ "\n------- Nova Rodada -----------\n"
							+ palavra.mascararPalavra(palavra.getPalavra(), letrasAcertadas,letrasUtilizadas));
				} else {
					//fim do jogo
					versao++;
					tela = montarTela("", versao, "\n------------------------------------------------------\r\nFim de jogo\r\n"
							+ jogador1 + " - " + pontosJogador1 + " ponto(s)\r\n"
							+ jogador2 + " - " + pontosJogador2 + " ponto(s)\r\n"
							+ jogador3 + " - " + pontosJogador3 + " ponto(s)\r\n"
							);
				}
				rodada++;
			} else {
				resposta = "Errou a palavra";
				String jogadorErrou = vezJogador;
				trocarVez();
				versao++;
				tela = montarTela(vezJogador, versao, "\n------------------------------------------------------\r\nO jogador " + jogadorErrou + " errou o chute e perdeu a vez\n"
						+ palavra.mascararPalavra(palavra.getPalavra(), letrasAcertadas,letrasUtilizadas));
			}
			
		}
		return resposta;
	}
	
	public void inicializaPalavras(){
        palavras = new Vector<Palavra>();
        palavras.add(new Palavra("animal","cachorro"));
        palavras.add(new Palavra("veiculo","carro"));
        palavras.add(new Palavra("fruta","morango"));
        palavras.add(new Palavra("objeto","cadeira"));
        palavras.add(new Palavra("objeto","mesa"));
        palavras.add(new Palavra("animal","gato"));
        palavras.add(new Palavra("animal","hipopotomo"));
        palavras.add(new Palavra("animal","elefante"));
        palavras.add(new Palavra("fruta","melancia"));
        palavras.add(new Palavra("veiculo","trem"));
        palavras.add(new Palavra("fruta","carambola"));
        palavras.add(new Palavra("objeto","teclado"));
        palavras.add(new Palavra("objeto","computador"));
        palavras.add(new Palavra("animal","papagaio"));
        palavras.add(new Palavra("animal","leopardo"));
        palavras.add(new Palavra("animal","leao"));
        palavras.add(new Palavra("fruta","pitanga"));
        palavras.add(new Palavra("veiculo","metro"));
        palavras.add(new Palavra("animal","galinha"));
        palavras.add(new Palavra("veiculo","bicicleta"));
        palavras.add(new Palavra("fruta","jaboticaba"));
        palavras.add(new Palavra("objeto","caneta"));
        palavras.add(new Palavra("objeto","armario"));
        palavras.add(new Palavra("animal","lagarto"));
        palavras.add(new Palavra("animal","peixe"));
        palavras.add(new Palavra("animal","orangotango"));
        palavras.add(new Palavra("fruta","laranja"));
        palavras.add(new Palavra("objeto","impressora"));
        palavras.add(new Palavra("objeto","espelho"));
        palavras.add(new Palavra("animal","pato"));
        palavras.add(new Palavra("animal","galo"));
        palavras.add(new Palavra("animal","tatu"));
        palavras.add(new Palavra("fruta","caja"));
        palavras.add(new Palavra("fruta","caju"));
        palavras.add(new Palavra("fruta","seriguela"));
        palavras.add(new Palavra("fruta","kiwi"));
        palavras.add(new Palavra("animal","girafa"));
        palavras.add(new Palavra("animal","rinoceronte"));
        palavras.add(new Palavra("animal","camelo"));
        palavras.add(new Palavra("animal","carneiro"));
   }
   
   Palavra getPalavra(int index){
	   return palavras.get(index); 
   }
   
   Vector<Palavra> getArrayPalavras(){
	   return palavras;
   }

}
